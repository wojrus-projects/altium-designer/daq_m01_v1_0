# Specyfikacja

Urządzenie **DAQ_M01** jest modułem pomiarowym (*Data Acquisition Module*) podłącznym do PC za pomocą interfejsu USB.

- 6 wejść (kanałów) napięciowych różnicowych (= 12 zacisków sygnałowych + 6 zacisków masy)
- ADC 24-bit Delta-Sigma
- Probkowanie równoległe (bez multipleksowania) we wszystkich włączonych kanałach
- Częstotliwości próbkowania: 250, 500, 1k, 2k, 4k, 8k, 16k, 32k samples/second
- Niezależna regulacja wzmocnienia w każdym wejściu różnicowym: CHANNEL_GAIN=1, 2, 4, 8, 16, 32, 64, 128 V/V
- Roboczy zakres napięć wejściowych względem masy: od -1.3 V do +3.3 V
- Maksymalny dopuszczalny zakres napięć wejściowych względem masy: od -12 V do +12 V (ciągle)
- Zakres napięć wejściowych różnicowych: V_diff=+/-1.2 V / CHANNEL_GAIN
- Pomocnicze wyjście napięcia referencyjnego: V_ref=+1.2 V
- Interfejs USB 2.0 FS (gniazdo typu B)
- Brak izolacji galwanicznej wejść analogowych względem USB
- LED "Status" RGB
- Zaciski sygnałowe śrubowe raster=5.0 mm
- Obudowa ABS IP40

# Projekt PCB

Schemat: [doc/DAQ_M01_V1_0_SCH.pdf](doc/DAQ_M01_V1_0_SCH.pdf)

Widok 3D: [doc/DAQ_M01_V1_0_3D.pdf](doc/DAQ_M01_V1_0_3D.pdf) (wymaga *Adobe Acrobat Reader DC*)

![](resource/pcb_3d.png "PCB 3D")

![](resource/pcb.png "PCB top")

# Firmware

Firmware do DAQ jest dostępne w repozytorium [embedded/stm32cubeide/stm32_workspace](https://gitlab.com/wojrus-projects/embedded/stm32cubeide/stm32_workspace).


# Oprogramowanie PC

Przykładowe oprogramowanie PC jest dostępne w repozytorium [python/labio](https://gitlab.com/wojrus-projects/python/labio).

# Licencja

MIT
